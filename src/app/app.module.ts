import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewGameComponent } from './components/new-game/new-game.component';
import { HomeComponent } from './components/home/home.component';
import { DelayInterceptorService } from './services/delay-interceptor.service';
import { GameStatisticsComponent } from './components/game-statistics/game-statistics.component';
import { SelectStartersComponent } from './components/select-starters/select-starters.component';

@NgModule({
  declarations: [
    AppComponent,
    NewGameComponent,
    HomeComponent,
    GameStatisticsComponent,
    SelectStartersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: DelayInterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
