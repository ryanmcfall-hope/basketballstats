import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

@Injectable()
export class DelayInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!environment.production) {
      console.log('Introducing delay into HttpRequest in non-production environment');
      return next.handle(req).pipe (delay(Math.random()*1500+500));
    }
    else {
      return next.handle(req);
    }
  }
}
