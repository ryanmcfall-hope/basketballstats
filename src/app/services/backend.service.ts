import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { pipe } from 'rxjs';
import { map, delay } from 'rxjs/operators';

import { Team } from '../models/team';
import { Player } from '../models/player';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private httpClient : HttpClient) {
    console.log("In BackendService constructor");
  }

  public getTeams() : Observable<Team[]> {
    return this.httpClient.get<object[]>("/api/teams").pipe(
      map(
        teams => {
          let teamArray = new Array<Team> ();
          teams.forEach (t => teamArray.push
            (
              new Team(t["id"], t["name"], t["mascot"], t["primaryColor"])
            )
          );
          return teamArray;
        }
      )
    );
  }

  public startGame (homeTeam : Team, awayTeam : Team) : Observable<number> {
    let httpHeaders = new HttpHeaders()
      .set('Content-type', 'application/json');

    let postBody = {
      "homeTeamId": homeTeam.Id,
      "awayTeamId": awayTeam.Id
    };

    return this.httpClient.post(
      "/api/games", postBody, { headers : httpHeaders}
    ).pipe (
      map(g => g["id"])
    );
  }

  public loadTeam (teamId : number) : Observable<Team> {
    return this.httpClient.get(`/api/teams/${teamId}?_embed=players`).pipe(
      map(t => {
        let team : Team = new Team(
          t["id"], t["name"], t["mascot"], t["primaryColor"]
        );
        this.loadPlayers(team, t["players"]);
        return team;
      })
    );
  }

  public loadTeamIds (gameId : number) : Observable<Array<number>> {
    return this.httpClient.get(`/api/games/${gameId}`).pipe(
      map(
        g => {
          let teamIds = new Array<number>();
          teamIds.push(g["homeTeamId"]);
          teamIds.push(g["awayTeamId"]);
          return teamIds;
        }
      )
    );
  }

  private loadPlayers (team : Team, players: Array<Player>) {
    players.forEach(p => {
      let player : Player = new Player(
        p["id"], p["jerseyNumber"], p["name"], p["heightInInches"]
      );
      team.addPlayer(player);
    });
  }
}
