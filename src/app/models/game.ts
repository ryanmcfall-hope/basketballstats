import { Team } from './team';
import { Player } from './player';
import { PlayerStats } from './player-stats';

export class Game {

	private homePlayers : Set<Player>;
	private awayPlayers : Set<Player>;

	private homeBench : Set<Player>;
	private awayBench : Set<Player>

	private statistics;

	public get HomeTeam() : Team {
		return this.homeTeam;
	}

	public set HomeTeam(t : Team) {
		this.homeTeam = t;
		this.statistics.set(this.homeTeam.Name, new Map());
		this.homeBench = new Set<Player> ();
		t.Players.forEach(p => this.homeBench.add(p));
	}

	public get AwayTeam() : Team {
		return this.awayTeam;
	}

	public set AwayTeam(t : Team) {
		this.awayTeam = t;
		this.statistics.set(this.awayTeam.Name, new Map());
		this.awayBench = new Set<Player>();
		t.Players.forEach(p => this.awayBench.add(p));
	}

	public constructor (
		private id : number, private homeTeam?: Team, private awayTeam? : Team,
		private homeScore = 0, private awayScore = 0
	) {
		this.homePlayers = new Set<Player>();
		this.awayPlayers = new Set<Player>();

		this.statistics = new Map();
	}

	public SubstituteIn (team : Team, playerIn : Player, playerOut? : Player) {
		let playerSet : Set<Player> = team == this.HomeTeam ? this.homePlayers : this.awayPlayers;
		let benchSet : Set<Player> = team == this.HomeTeam ? this.homeBench : this.awayBench;

		if (playerOut) {
			playerSet.delete(playerOut);
			benchSet.add(playerOut);
		}
		playerSet.add(playerIn);
		benchSet.delete(playerIn);
	}



}
