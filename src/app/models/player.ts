export class Player {
	private _number : number;
	private _name : string;
	private _heightInInches : number;
	private _id : number;

	get id() : number {
		return this._id;
	}

	set id (newId : number) {
		this._id = newId;
	}

	get JerseyNumber() : number {
		return this._number;
	}

	set JerseyNumber(newNum : number) {
		this._number = newNum;
	}

	get Name() : string {
		return this._name;
	}

	set Name(newName : string) {
		this._name = newName;
	}

	get HeightInInches() : number {
		return this._heightInInches;
	}
	set HeightInches(newHeight : number) {
			this._heightInInches = newHeight;
	}

	get Height() : string {
		return Math.floor(this.HeightInches / 12).toString() + "' '" +
		       (this.HeightInches % 12).toString() + "\"";
	}

	constructor (_id: number, num : number, name: string, heightInches : number) {
		this.JerseyNumber = num;
		this.Name = name;
		this.HeightInches = heightInches;
		this.id = _id;
	}
}
