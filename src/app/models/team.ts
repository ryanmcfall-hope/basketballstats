import { Player } from './player';

export class Team {

	get Id () : number {
		return this.id;
	}

	get Name () : string {
		return this.name;
	}

	set Name(_name : string) {
		this.name = _name;
	}

	get Mascot() : string {
		return this.mascot;
	}

	set Mascot(newMascot : string) {
		this.mascot = newMascot;
	}

  get Players() : Player[] {
		return this.players;
	}

	get SortedPlayers() : Player[] {
		return this.players.sort((p1, p2) => p1.JerseyNumber - p2.JerseyNumber);
	}

	get PrimaryColor() : string {
		return this.primaryColor;
	}

	private players : Player[];

	constructor (private id: number, private name : string, private mascot : string, private primaryColor : string) {
		this.players = new Array<Player> ();
	}

	addPlayer (player : Player) {
		this.players.push (player);
	}

}
