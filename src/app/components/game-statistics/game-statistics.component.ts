import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { concat } from 'rxjs';

import { BackendService } from '../../services/backend.service';
import { Game } from '../../models/game';

@Component({
  selector: 'app-game-statistics',
  templateUrl: './game-statistics.component.html',
  styleUrls: ['./game-statistics.component.scss']
})
export class GameStatisticsComponent implements OnInit {

  private gameId : number;
  private game : Game;

  status : string;
  numTeamsStartersSet : number;

  constructor(private activatedRoute : ActivatedRoute, private backendService: BackendService) { }

  ngOnInit() {
    this.gameId = +this.activatedRoute.snapshot.paramMap.get('id');
    this.game = new Game (this.gameId);
    console.log(`GameID in GameStatisticsComponent is ${this.gameId}`);

    this.status = 'load-game';
    this.numTeamsStartersSet = 0;

    this.backendService.loadTeamIds(this.gameId).subscribe(
      ids => this.loadTeams(ids)
    );

  }

  private loadTeams (teamIds : Array<number>) {
    this.status = 'load-teams';
    let teamRequests = concat(
      this.backendService.loadTeam(teamIds[0]),
      this.backendService.loadTeam(teamIds[1])
    );

    teamRequests.subscribe( t=> {
      console.log(t);
      if (t.Id == teamIds[0]) {
        this.game.HomeTeam = t;
      }
      else {
        this.game.AwayTeam = t;
        this.status = 'teams-loaded';
      }
    });
  }

  setStarters(team, starters) : void {
    starters.forEach(player => this.game.SubstituteIn(team, player));
    this.numTeamsStartersSet++;
    if (this.numTeamsStartersSet == 2) {
      this.status = 'in-game';
    }
  }

}
