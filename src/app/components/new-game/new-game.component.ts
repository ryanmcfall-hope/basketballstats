import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BackendService } from '../../services/backend.service';
import { Team } from '../../models/team';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent implements OnInit {

  teams : Team[];

  homeTeam: Team;
  awayTeam: Team;

  constructor(private backend : BackendService, private router : Router) { }

  ngOnInit() {
    this.backend.getTeams().subscribe(teams => this.teams = teams);
  }

  startGame() {
    console.log("Home team: ");
    console.log(this.homeTeam);
    console.log("Away team: ");
    console.log(this.awayTeam);
    this.backend.startGame(this.homeTeam, this.awayTeam).subscribe(
      gameId => {
        console.log(`Created game with ID ${gameId}`);
        this.router.navigateByUrl(`/gamestats/${gameId}`);
      }
    )
  }
}
