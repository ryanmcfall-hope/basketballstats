import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectStartersComponent } from './select-starters.component';

describe('SelectStartersComponent', () => {
  let component: SelectStartersComponent;
  let fixture: ComponentFixture<SelectStartersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectStartersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectStartersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
