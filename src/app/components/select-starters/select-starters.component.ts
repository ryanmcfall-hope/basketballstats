import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Team } from '../../models/team';
import { Player } from '../../models/player';

@Component({
  selector: 'app-select-starters',
  templateUrl: './select-starters.component.html',
  styleUrls: ['./select-starters.component.scss']
})
export class SelectStartersComponent implements OnInit {

  @Input()
  private team : Team;

  @Output()
  startersSelected : EventEmitter<Set<Player>> = new EventEmitter<Set<Player>>();

  private starters : Set<Player>;

  private startersSet : boolean;

  constructor() { }

  ngOnInit() {
    this.starters = new Set<Player>();
    this.startersSet = false;
  }

  toggleStarter(player : Player) {
    console.log(`Toggling whether ${player.Name} is a starter`);
    if (this.starters.has(player)) {
      console.log (`Removing ${player.Name}`);
      this.starters.delete(player);
    }
    else {
      console.log (`Adding ${player.Name}`);
      this.starters.add(player);
    }
  }

  setStarters () : void {
    this.startersSet = true;
    this.startersSelected.emit(this.starters);
  }

  StartersSelected() : number {
    return this.starters.size;
  }



}
