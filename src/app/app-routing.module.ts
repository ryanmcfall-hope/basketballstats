import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewGameComponent } from './components/new-game/new-game.component';
import { HomeComponent } from './components/home/home.component';
import { GameStatisticsComponent } from './components/game-statistics/game-statistics.component';

const routes: Routes = [
  {'path': '', 'component': HomeComponent},
  {'path': 'newgame', 'component': NewGameComponent},
  {'path': 'gamestats/:id', 'component': GameStatisticsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
